#!/bin/bash 
export BUILD_IMAGENAME=cvent-6-connex
export BUILD_SOURCEVERSION=1.0.2-snapshot

# Delete Stopped Containers
docker rm $(docker ps -a -q)

# Prune Dangling Images 
docker image prune -f

# Clean Install (This will only setup the Target Folder Compose, Will NOT create image anymore)
docker build --tag=wireframesln/${BUILD_IMAGENAME}:${BUILD_SOURCEVERSION} --rm=true .
docker push wireframesln/${BUILD_IMAGENAME}:${BUILD_SOURCEVERSION}
