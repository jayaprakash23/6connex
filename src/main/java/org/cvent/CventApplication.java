package org.cvent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.config.EnableWebFlux;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableR2dbcRepositories(basePackageClasses = ReactiveQueryByExampleExecutor.class)
@SpringBootApplication
@EnableSwagger2
@EnableWebFlux
@ComponentScan(basePackages = {"org.cvent"})
@EnableAutoConfiguration
@EnableScheduling
public class CventApplication {

    public static void main(String[] args) {
        SpringApplication.run(CventApplication.class, args);
    }

}