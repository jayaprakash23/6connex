package org.cvent.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.ClientResponse;

@Data
@EqualsAndHashCode(callSuper = false)
public class AppException extends Exception {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppException.class);
    /**
     * default serial version id.
     */
    private static final long serialVersionUID = 1L;

    private String errorDesc;

    /**
     * Instantiates a new apex exception.
     *
     * @param errorDesc
     * @param th
     */
    public AppException(String errorDesc, Throwable th) {
        super(th);
        this.errorDesc = errorDesc;
    }

    public AppException(String errorDesc, ClientResponse clientResponse) {
        clientResponse.bodyToMono(String.class).doOnNext(aa -> LOGGER.info("value {}", aa))
                .subscribe();
        this.errorDesc = errorDesc;
    }

    public AppException(String errorDesc, String clientResponse) {
        LOGGER.error("clientResponse {} ", clientResponse);
        this.errorDesc = clientResponse;
    }
}
