package org.cvent.bean.attendee;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.cvent.bean.event.CustomFields;

import java.util.List;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String company;
    private String mobilePhone;
    private String sourceId;
    private String primaryAddressType;
    private String start;
    private String createdBy;
    private String lastModifiedBy;
    private Boolean deleted;
    private String title;
    private String designation;
    private String ccEmail;
    private String gender;
    private String workPhone;
    private String workFax;
    private String middleName;
    private List<CustomFields> customFields;
    private HomeAddress homeAddress;
    private WorkAddress workAddress;
    private Type type;
}
