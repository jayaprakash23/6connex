package org.cvent.bean.attendee;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HomeAddress {
    private String region;
    private String regionCode;
    private String country;
    private String latitude;
    private String longitude;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String countryCode;
    private String postalCode;

}
