package org.cvent.bean.attendee;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.cvent.bean.event.CustomFields;

import javax.xml.datatype.XMLGregorianCalendar;

import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AttendeeData {
    private String id;
    private String confirmationNumber;
    private boolean unsubscribed;
    private String status;
    private String invitedBy;
    private String responseMethod;
    private boolean testRecord;
    private boolean showPopupNotification;
    private boolean allowPushNotifications;
    private boolean allowAppointmentPushNotifications;
    private String visibility;
    private String bio;
    private String websiteUrl;
    private String createdBy;
    private String lastModifiedBy;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar created;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar lastModified;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar registrationLastModified;

    private Event event;
    private Contact contact;
    private InvitationList invitationList;
    private RegistrationPath registrationPath;
    private RegistrationType registrationType;
    private AdmissionItem admissionItem;
    private  List<Questions> questions;
    private List<Answers> answers;
}