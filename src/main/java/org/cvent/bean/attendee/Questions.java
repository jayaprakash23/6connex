package org.cvent.bean.attendee;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Questions {
    private String id;
    private String name;
    private String type;
    private ArrayList value;
    private String order;

}
