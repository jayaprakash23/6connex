package org.cvent.bean.attendee;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetAttendeeResponseBean {

    private List<AttendeeData> data;
}

