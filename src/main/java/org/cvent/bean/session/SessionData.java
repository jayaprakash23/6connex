package org.cvent.bean.session;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SessionData {
    private String id;
    private String virtual;
    private String code;
    private String title;

    private String description;
    private Boolean includedSession;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar start;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar end;
    private String status;
    private Boolean enableWaitlist;
    private Boolean enableWaitlistVirtual;
    private Long capacity;
    private Boolean capacityUnlimited;
    private Long capacityVirtual;
    private Boolean virtualCapacityUnlimited;
    private Long waitlistCapacityVirtual;
    private String timezone;
    private Boolean featured;
    private Boolean openForRegistration;
    private Boolean openForAttendeeHub;
    private ArrayList registrationTypes;
    private String presentationType;
    private String dataTagCode;

    private String createdBy;
    private String lastModifiedBy;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar created;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar lastModified;
    private List<CustomFields> customFields;
    private Event event;
    private Location location;
    private Type type;
    private Category category;

}


