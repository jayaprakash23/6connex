package org.cvent.bean.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdmissionData {
    private String id;
    private String name;
    private String code;
    private String description;
    private boolean allowOptionalSessions;
    private AdmissionEvent admissionEvent;
}


