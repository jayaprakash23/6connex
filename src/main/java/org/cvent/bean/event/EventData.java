package org.cvent.bean.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

import java.util.ArrayList;
import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventData {
    private String id;
    private String title;
    private String code;
    private String description;
    private String start;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar end;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar closeAfter;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar archiveAfter;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar launchAfter;
    private String timezone;
    private String defaultLocale;
    private String currency;
    private String registrationSecurityLevel;
    private String status;
    private String eventStatus;
    private String planningStatus;
    private boolean testMode;
    private Long capacity;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar created;
    @JsonFormat(pattern = dateTimeFormat)
    protected XMLGregorianCalendar lastModified;
    private boolean virtual;
    private String format;
    private ArrayList languages;
    private List<Planners> planers;
    private List<CustomFields> customFields;
    private List<Venues> venues;
    private Category category;

}


