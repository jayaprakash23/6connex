package org.cvent.bean.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Planners {
    private String firstName;
    private String lastName;
    private String email;
}
