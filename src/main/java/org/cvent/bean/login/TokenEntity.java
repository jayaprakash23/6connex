package org.cvent.bean.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenEntity {

    private Long id;
    private String access_token;
    private Long expires_in;
    private String refresh_token;
    private String token_type;

}

