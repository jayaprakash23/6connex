package org.cvent.bean.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.cvent.util.AppConstants;


@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CodeBean {

    public String grant_type;
    public String refresh_token;
    public String scope;
    public String redirect_uri;
    public String code;
    public String client_id;


    public String toString() {
        return "grant_type" + AppConstants.EQUAL + grant_type + AppConstants.AND + "scope" + AppConstants.EQUAL + scope;
    }


}
