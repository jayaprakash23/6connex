package org.cvent.bean.login;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

import static org.cvent.config.Config.dateTimeFormat;


@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenBean {
    public Long id;
    public String client_id;
    public String client_secret;
    public String grant_type;
    public String refreshToken;
    private Long refreshTokenExpiresIn;
    public String scope;
    public String code;
    @JsonFormat(pattern = dateTimeFormat)
    protected LocalDateTime createdDate;


}
