package org.cvent.bean.registration;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact {

    private List<ContactDto> contact;

}

