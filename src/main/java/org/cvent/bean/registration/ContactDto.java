package org.cvent.bean.registration;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import static org.cvent.config.Config.dateTimeFormat;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactDto {
    private List<CustomFieldDetail> customFieldDetail;

    private HomeAddress homeAddress;
    private WorkAddress workAddress;


    private String id;
    private Long parentId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String ccEmail;
    private String gender;
    private String company;
    private String designation;
    private String title;
    private Long mobilePhone;
    private String nickname;
    private String workPhone;
    private String workFax;
    @JsonFormat(pattern = dateTimeFormat)
    private LocalDateTime lastModified;
    private String createdBy;
    private String lastModifiedBy;

}

