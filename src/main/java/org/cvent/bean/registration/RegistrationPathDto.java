package org.cvent.bean.registration;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegistrationPathDto {
    private Long id;
    private String code;
    private String name;
}
