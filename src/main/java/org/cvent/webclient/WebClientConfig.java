package org.cvent.webclient;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.tcp.TcpClient;

import java.time.Duration;
import java.util.List;

@Configuration
public class WebClientConfig {
    private static final Logger logger = LoggerFactory.getLogger(WebClientConfig.class);

    @Bean
    public WebClient defaultWebClient() {
        logger.info("Initiating Custom webclient....");
        ConnectionProvider provider =
                ConnectionProvider.builder("fixed")
                        .maxConnections(80)
                        .maxIdleTime(Duration.ofMillis(200))
                        .pendingAcquireMaxCount(800)
                        .pendingAcquireTimeout(Duration.ofMinutes(20))
                        .build();

        TcpClient tcpClient = TcpClient.create(provider)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 300000)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(120))
                                .addHandlerLast(new WriteTimeoutHandler(50)));

        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs()
                        .maxInMemorySize(1024 * 1024 * 10 * 10)).build();

        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .exchangeStrategies(exchangeStrategies)
                .filter(logRequest())
                .filter(logResponse())
                .build();
    }
    private ExchangeFilterFunction logRequest() {
        return (clientRequest, next) -> {
            StringBuilder sb = new StringBuilder();
            sb.append("===========================Request begin================================================\n")
                    .append("URI         : ").append(clientRequest.url()).append("\n")
                    .append("Method         : ").append(clientRequest.method()).append("\n")
                    .append("==========================Request end================================================");
            logger.info(sb.toString());
            return next.exchange(clientRequest);
        };
    }

    private ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            StringBuilder sb = new StringBuilder();
            sb.append("===========================Response begin================================================\n")
                    .append("Status Code         : ").append(clientResponse.statusCode()).append("\n")
                    .append("==========================Response end================================================");
            logger.info(sb.toString());
            return Mono.just(clientResponse);
        });
    }

    private void logHeader(String name, List<String> values) {
        values.forEach(value -> logger.info("{}={}", name, value));
    }
}
