package org.cvent.webclient;


import org.cvent.bean.GetResponseBean;
import org.cvent.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class WebClientWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebClientWrapper.class);

    private final WebClient appWebClient;

    public WebClientWrapper(WebClient appWebClient) {
        this.appWebClient = appWebClient;
    }

    /**
     * @param url
     * @param apiRequest
     * @param mediaType
     * @param <T>
     * @return
     */
    public <T> Mono<String> postListToAPI(String url,
                                          List<T> apiRequest,
                                          MediaType mediaType) {
        LOGGER.info("POST LIST URL [{}] and MESSAGE [{}] ", url, apiRequest);
        return this.appWebClient
                .post()
                .uri(url)
                .contentType(mediaType)
                .body(BodyInserters.fromValue(apiRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(String.class);

    }

    /**
     * @param url
     * @param response
     * @param authToken
     * @param <R>
     * @param <T>
     * @return
     */
    public <R, T> Mono<R> getFromAPI(String url, Class<R> response, String authToken) {
        return this.appWebClient.get()
                .uri(url)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

    /**
     * @param url
     * @param response
     * @param <R>
     * @param <T>
     * @return
     */
    public <R, T> Mono<R> getFromAPI(String url, Class<R> response) {
        return this.appWebClient.get()
                .uri(url)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response)
                .log("response");
    }

    /**
     * post api to multipart
     *
     * @param url
     * @param apiRequest
     * @param cl
     * @param mediaType
     * @param <R>
     * @return
     */
    public <R> Mono<R> postAPIToMultiPart(String url,
                                          LinkedMultiValueMap<String, String> apiRequest,
                                          Class<R> cl,
                                          MediaType mediaType) {

        LOGGER.info("POST Multipart URL [{}] and MESSAGE [{}] ", url, apiRequest);
        return this.appWebClient
                .post()
                .uri(url)
                .contentType(mediaType)
                .bodyValue(apiRequest)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(cl);
    }

    /**
     * Json post api
     *
     * @param url
     * @param request
     * @param response
     * @param authToken
     * @param method
     * @param <R>
     * @param <T>
     * @return
     */
    public <R, T> Mono<R> postToAPI(String url, T request, Class<R> response, String authToken, HttpMethod method) {
        LOGGER.info("URL [{}] and MESSAGE [{}] ", url, request);
        return this.appWebClient
                .method(method)
                .uri(url)
                //.header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .bodyValue(request)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

    /**
     * @param url
     * @param request
     * @param response
     * @param authToken
     * @param <R>
     * @param <T>
     * @return
     */
    public <R, T> Mono<R> putToAPI(String url, T request, Class<R> response, String authToken) {
        LOGGER.info("URL [{}] and MESSAGE [{}] ", url, request);
        return this.appWebClient
                .put()
                .uri(url)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

    /**
     * @param clientResponse
     * @return
     */
    private Mono<? extends Throwable> error4xxHandling(ClientResponse clientResponse) {
        LOGGER.error("400 Error Received, and response is {} ", clientResponse.statusCode());
        return clientResponse.bodyToMono(String.class).flatMap(response ->
                Mono.error(new AppException("Bad Error", response)));
    }

    /**
     * @param clientResponse
     * @return
     */
    private Mono<? extends Throwable> error5xxHandling(ClientResponse clientResponse) {
        LOGGER.error("500 Error Received, and response is {} ", clientResponse.statusCode());
        return Mono.error(new AppException("Internal Server Error", clientResponse));
    }

    public <R, T> Mono<R> postToTokenAPI(String url, T request, Class<R> response, String authToken, HttpMethod method) {
        LOGGER.info("URL [{}] and MESSAGE [{}] ", url, request);
        return this.appWebClient
                .method(method)
                .uri(url)
                .header(HttpHeaders.CONTENT_TYPE, "" + MediaType.APPLICATION_FORM_URLENCODED)
                .header(HttpHeaders.AUTHORIZATION, "Basic " + authToken)
                .bodyValue(request)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

    //    public <R, T> Mono<R> postToAPI(String url, T request, Class<R> response, String authToken, HttpMethod method) {
    public <R, T> Mono<R> postToContactAPI(String url, T request, Class<R> response, String authToken, HttpMethod method) {
        LOGGER.info("URL [{}] and MESSAGE [{}] ", url, request);
        return this.appWebClient
                .method(method)
                .uri(url)
                //.header(HttpHeaders.CONTENT_TYPE, "" + MediaType.APPLICATION_FORM_URLENCODED)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .bodyValue(request)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

    public <R, T> Mono<R> postToAPIAttendee(String url, Object request, Class<GetResponseBean> response, String authToken, HttpMethod method) {
        LOGGER.info("URL [{}] and MESSAGE [{}] ", url, request);
        return (Mono<R>) this.appWebClient
                .method(method)
                .uri(url)
                //.header(HttpHeaders.CONTENT_TYPE, "" + MediaType.APPLICATION_FORM_URLENCODED)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .bodyValue(request)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }


    public <R, T> Mono<R> getFromAPICvent(String url, Class<R> response, String authToken, HttpMethod method) {
        return this.appWebClient
                .method(method)
                .uri(url)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authToken)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::error4xxHandling)
                .onStatus(HttpStatus::is5xxServerError, this::error5xxHandling)
                .bodyToMono(response);
    }

}
