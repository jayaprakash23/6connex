package org.cvent.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;

@Entity
@Table(value = "CVENT_SESSION")
@Data
@DynamicUpdate
public class SessionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "LOCAL_SESSION_ID")
    private Long id;
    @Column(value = "CLIENT_ID")
    private String clientId;
    @Column(value = "SESSION_STUB")
    private String sessionId;
    @Column(value = "EVENT_STUB")
    private String eventId;
    @Column(value = "EVENT_TITLE")
    private String productName;
    @Column(value = "PRODUCT_CODE")
    private String productCode;
    @Column(value = "PRODUCT_TYPE")
    private String productType;
    @Column(value = "EVENT_START_DATE")
    private LocalDateTime startTime;
    @Column(value = "EVENT_END_DATE")
    private LocalDateTime endTime;
    @Column(value = "DATA_TAG_CODE")
    private String dataTagCode;
    @Column(value = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Column(value = "SESSION_LOCATION_NAME")
    private String sessionLocationName;
    @Column(value = "STATUS")
    private String status;
    /*@Column(value = "SESSION_LOCATION_CODE")
    private String sessionLocationCode;
    @Column(value = "REGISTRANT_INFORMATION")
    private String registrantInformation;
    @Column(value = "CAPACITY")
    private int capacity;
    @Column(value = "ENABLE_WAIT_LIST")
    private boolean enableWaitList;
    @Column(value = "WAIT_LIST_CAPACITY")
    private int waitListCapacity;
    @Column(value = "AUTO_CLOSE_DATE")
    private LocalDateTime autoCloseDate;
    @Column(value = "UPDATED_FLAG")
    private String updatedFlag;
    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "CREATED_DATE")
    private LocalDateTime createdDate;*/

    @Column(value = "UPDATED_BY")
    private String updatedBy;
    @Column(value = "UPDATED_DATE")
    private LocalDateTime updatedDate;
    @Column(value = "PLATFORM_CO_HOST")
    private String platformCoHost;
    @Column(value = "EVENT_PLATFORM")
    private String eventPlatform;
    @Column(value = "PLATFORM_SUB_ACCOUNT")
    private String platformSubAccount;
    @Column(value = "REQUESTED_ACTION")
    private String requestedAction;
    @Column(value = "HOLD_REG_FLOW")
    private String holdRegFlow;
    @Column(value = "PLATFORM_EVENT_ID")
    private String platformEventId;
    @Column(value = "TIME_ZONE")
    private String timeZone;
    @Column(value = "PARTICIPATION_DURATION")
    private String participationDuration;
}
