package org.cvent.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;

@Entity
@Table(value = "CVENT_SESSION_REGISTRATION")
@Data
public class SessionRegistrationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "SESSION_REGISTRATION_ID")
    private long sessionRegistrationId;

    @Column(value = "CLIENT_ID")
    private String clientId;
    @Column(value = "EVENT_STUB")
    private String eventId;
    @Column(value = "INVITEE_STUB")
    private String inviteeId;
    @Column(value = "ORDER_DETAIL_ID")
    private String orderDetailId;
    @Column(value = "FIRST_NAME")
    private String firstName;
    @Column(value = "LAST_NAME")
    private String lastName;
    @Column(value = "SESSION_STUB")
    private String productId;
    @Column(value = "PRODUCT_NAME")
    private String productName;
    /*@Column(value = "ORDER_DETAIL_ITEM_ID")
    private String orderDetailItemId;
    @Column(value = "ORDER_NUMBER")
    private String orderNumber;
    @Column(value = "PRODUCT_CODE")
    private String productCode;
    @Column(value = "PRODUCT_TYPE")
    private String productType;
    @Column(value = "QUANTITY")
    private int quantity;
    @Column(value = "PARTICIPANT")
    private boolean participant;
    @Column(value = "UPDATED_FLAG")
    private String updatedFlag;
    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "CREATED_DATE")
    private LocalDateTime createdDate;
    */

    @Column(value = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Column(value = "START_TIME")
    private LocalDateTime startTime;
    @Column(value = "END_TIME")
    private LocalDateTime endTime;
    @Column(value = "UPDATED_BY")
    private String updatedBy;
    @Column(value = "UPDATED_DATE")
    private LocalDateTime updatedDate;

    @Column(value = "EVENT_PLATFORM")
    private String eventPlatform;

    @Column(value = "REQUESTED_ACTION")
    private String requestedAction;

    @Column(value = "COUNTRY")
    private String country;
    @Column(value = "COMPANY")
    private String company;
    @Column(value = "TITLE")
    private String title;
    @Column(value = "EMAIL_ADDRESS")
    private String emailAddress;
    @Column(value = "CC_EMAIL_ADDRESS")
    private String ccEmailAddress;
    @Column(value = "CONTACT_STUB")
    private String contactId;
    @Column(value = "SOURCE_STUB")
    private String sourceId;
    @Column(value = "REGISTRATION_TYPE")
    protected String registrationType;
    @Column(value = "SURVEY_QUESTION1")
    private String surveyQuestion1;
    @Column(value = "SURVEY_QUESTION2")
    private String surveyQuestion2;
    @Column(value = "SURVEY_QUESTION3")
    private String surveyQuestion3;
    @Column(value = "SURVEY_QUESTION4")
    private String surveyQuestion4;
    @Column(value = "SURVEY_QUESTION5")
    private String surveyQuestion5;
    @Column(value = "SURVEY_QUESTION6")
    private String surveyQuestion6;
    @Column(value = "SURVEY_QUESTION7")
    private String surveyQuestion7;
    @Column(value = "IMAGE_URL")
    protected String imageURL;
    @Column(value = "GENDER")
    protected String gender;
    @Column(value = "WORK_ADDRESS1")
    protected String address1;
    @Column(value = "WORK_ADDRESS2")
    protected String address2;
    @Column(value = "WORK_CITY")
    protected String workCity;
    @Column(value = "WORK_STATE")
    protected String workState;
    @Column(value = "WORK_POSTAL_CODE")
    protected String workPostalCode;
    @Column(value = "WORK_PHONE")
    private String workPhone;
    @Column(value = "STATUS")
    private String status;


}
