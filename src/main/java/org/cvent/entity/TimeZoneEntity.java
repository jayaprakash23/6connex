package org.cvent.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;


@Entity
@Data
@Table(value = "cvent_timezone_mapping")
public class TimeZoneEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "ID")
    private Long id;

    @Column(value = "TIME_ZONE_NAME")
    private String timeZoneName;

    @Column(value = "DAYLIGHT_OFFSET")
    private String daylightOffset;

    @Column(value = "STANDARD_OFFSET")
    private String standardOffset;

    @Column(value = "CREATED_DATE")
    private LocalDateTime createdDate;
}
