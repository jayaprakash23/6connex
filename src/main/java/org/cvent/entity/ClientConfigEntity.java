package org.cvent.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@Entity
@Data
@Table(value = "CVENT_CLIENT_CONFIG")
public class ClientConfigEntity {


    @Id
    @Column(value = "CLIENT_ID")
    private Long clientId;
    @Column(value = "CLIENT_CONFIG_ID")
    private Long clientConfigId;
    @Column(value = "IS_EVENT")
    private String isEvent;
    @Column(value = "IS_REG")
    private String isReg;
    @Column(value = "IS_SESSION_REG")
    private String isSessionReg;
    @Column(value = "IS_GUEST")
    private String isGuesProcess;
    @Column(value = "IS_SESSION")
    private String isSession;
    @Column(value = "IS_REG_CONTACT")
    private String isRegContact;
    @Column(value = "IS_SESSION_NAME")
    private String isSessionName;

    @Column(value = "SL_EVENT_PLATFORM")
    private String slEventPlatform;
    @Column(value = "SL_PLATFORM_EVENT_ID")
    private String slPlatformEventId;
    @Column(value = "SL_PLATFORM_SUB_ACCOUNT")
    private String slPlatformSubAccount;
    @Column(value = "SL_PLATFORM_CO_HOST")
    private String slPlatformCoHost;
    @Column(value = "SL_HOLD_REG_FLOW")
    private String slHoldRegFlow;
    @Column(value = "SL_TEAM_NAME")
    private String slTeamName;
    @Column(value = "SL_PARTICIPANT_DURATION")
    private String slParticipantDuration;

    @Transient
    private String reconcileSessionReg;
    @Transient
    private String reconcileSessionLevelReg;
    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "UPDATED_BY")
    private String updatedBy;
    @CreationTimestamp
    @Column(value = "CREATED_DATE")
    private LocalDateTime createdDate;
    @UpdateTimestamp
    @Column(value = "UPDATED_DATE")
    private LocalDateTime updatedDate;
}
