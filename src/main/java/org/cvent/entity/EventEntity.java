package org.cvent.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;


@Entity
@Data
@Table(value = "CVENT_EVENT")
public class EventEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "ID")
    private long id;
    @Column(value = "EVENT_STUB")
    private String eventStub;
    @Column(value = "CLIENT_ID")
    private String clientId;
    @Column(value = "EVENT_CODE")
    private String eventCode;
    @Column(value = "EVENT_TITLE")
    private String eventTitle;
    @Column(value = "EVENT_STATUS")
    private String eventStatus;
    @Column(value = "PLANNING_STATUS")
    private String planningStatus;
    @Column(value = "EVENT_START_DATE")
    private LocalDateTime eventStartDate;
    @Column(value = "EVENT_END_DATE")
    private LocalDateTime eventEndDate;
    @Column(value = "LOCATION")
    private String location;
    @Column(value = "CITY")
    private String city;
    @Column(value = "STREET_ADDRESS_1")
    private String streetAddress1;
    @Column(value = "STREET_ADDRESS_2")
    private String streetAddress2;
    @Column(value = "STREET_ADDRESS_3")
    private String streetAddress3;
    @Column(value = "PLANNER_FIRST_NAME")
    private String plannerFirstName;
    @Column(value = "PLANNER_LAST_NAME")
    private String plannerLastName;
    @Column(value = "PLANNER_EMAIL_ADDRESS")
    private String plannerEmailAddress;
    @Column(value = "EVENT_DESCRIPTION")
    private String eventDescription;
    @Column(value = "LAST_MODIFIED_DATE")
    private LocalDateTime lastModifiedDate;
    @Column(value = "TIME_ZONE")
    private String timeZone;
    @Column(value = "EVENT_PLATFORM")
    private String eventPlatform;
    @Column(value = "PLATFORM_CO_HOST")
    private String platformCoHost;
    @Column(value = "PLATFORM_SUB_ACCOUNT")
    private String platformSubAccount;
    @Column(value = "REQUESTED_ACTION")
    private String requestedAction;
    @Column(value = "PLATFORM_EVENT_ID")
    private String platformEventId;
    @Column(value = "UPDATED_FLAG")
    private String updatedFlag;
    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "UPDATED_BY")
    private String updatedBy;
    @Column(value = "CREATED_DATE")
    private LocalDateTime createdDate;
    @Column(value = "UPDATED_DATE")
    private LocalDateTime updatedDate;
    @Column(value = "HOLD_REG_FLOW")
    private String holdRegFlow;
    @Column(value = "PARTICIPATION_DURATION")
    private String participationDuration;
    @Column(value = "TEAM_NAME")
    private String teamName;
    @Column(value = "SL_CUSTOM_FIELD1")
    private String slCustomField1;
}
