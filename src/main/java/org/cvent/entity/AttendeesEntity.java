package org.cvent.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "ATTENDEES")

public class AttendeesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "ID")
    private Long id;

    @Column(value = "ATTENDEES_INFO")
    public String attendeesInfo;


}