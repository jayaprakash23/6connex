package org.cvent.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;

import static org.cvent.config.Config.dateTimeFormat;

@Entity
@Data
@Table(value = "CONTACTS")

public class ContactsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "CONTACT_ID")
    private Long contactId;

    @Column(value = "ID")
    private String id;
    @Column(value = "NAME")
    private String name;
    @Column(value = "SHORT_DESCRIPTION")
    private String shortDescription;
    @Column(value = "DESCRIPTION")
    private String description;
    @Column(value = "TYPE")
    private String type;
    @Column(value = "NOTE")
    private String note;
    @CreationTimestamp
    @JsonFormat(pattern = dateTimeFormat)
    @Column(value = "CREATED_DATE")
    private LocalDateTime created;
    @UpdateTimestamp
    @JsonFormat(pattern = dateTimeFormat)
    @Column(value = "UPDATED_DATE")
    private LocalDateTime lastModified;
    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "UPDATED_BY")
    private String lastModifiedBy;


}