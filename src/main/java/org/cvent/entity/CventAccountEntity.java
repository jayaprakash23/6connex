package org.cvent.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;

import static org.cvent.config.Config.dateTimeFormat;

@Entity
@Data
@Table(value = "CVENT_ACCOUNT")
public class CventAccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(value = "ID")
    private Long id;
    @Column(value = "CLIENT_ID")
    private String client_id;
    @Column(value = "CLIENT_SECRET")
    private String client_secret;
    @Column(value = "SCOPE")
    private String scope;
    @Column(value = "GRANT_TYPE")
    private String grant_type;
    //token save columns
    @Column(value = "REFRESH_TOKEN")
    private String refresh_token;
    @Column(value = "ACCESS_TOKEN")
    private String accessToken;
    @Column(value = "TOKEN_TYPE")
    private String tokenType;
    @Column(value = "EXPIRES_IN")
    private Long expiresIn;

    @Column(value = "CREATED_BY")
    private String createdBy;
    @Column(value = "UPDATED_BY")
    private String updatedBy;
    @UpdateTimestamp
    @JsonFormat(pattern = dateTimeFormat)
    @Column(value = "UPDATED_DATE")
    private LocalDateTime updatedDate;


}
