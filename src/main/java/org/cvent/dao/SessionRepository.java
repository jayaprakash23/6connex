package org.cvent.dao;

import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.SessionEntity;

public interface SessionRepository extends AppBaseRepository<SessionEntity, Long> {
}
