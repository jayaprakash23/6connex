package org.cvent.dao.custom;

import org.cvent.entity.TimeZoneEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * TimeZoneRepository Interface to perform DB Operations
 */
public interface TimeZoneRepository extends CrudRepository<TimeZoneEntity, Long> {

}
