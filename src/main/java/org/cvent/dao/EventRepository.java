package org.cvent.dao;

import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.EventEntity;

public interface EventRepository extends AppBaseRepository<EventEntity, Long> {
}
