package org.cvent.dao;


import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.CventAccountEntity;

public interface CventAccountRepository extends AppBaseRepository<CventAccountEntity, Long> {


}
