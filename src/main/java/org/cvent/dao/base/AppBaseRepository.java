package org.cvent.dao.base;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AppBaseRepository<T, ID> extends R2dbcRepository<T, ID>{
}
