package org.cvent.dao;

import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.AttendeesEntity;

public interface AttendeesRepository extends AppBaseRepository<AttendeesEntity, Long> {
}
