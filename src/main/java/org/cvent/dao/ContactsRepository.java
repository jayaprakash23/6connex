package org.cvent.dao;

import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.ContactsEntity;

public interface ContactsRepository extends AppBaseRepository<ContactsEntity, Long> {
}
