package org.cvent.dao;

import org.cvent.dao.base.AppBaseRepository;
import org.cvent.entity.SessionRegistrationEntity;

public interface SessionRegistrationRepository extends AppBaseRepository<SessionRegistrationEntity, Long>{
}
