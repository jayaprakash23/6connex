package org.cvent.controller;


import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.ResponseBean;
import org.cvent.entity.ContactsEntity;
import org.cvent.entity.CventAccountEntity;
import org.cvent.entity.PageSupport;
import org.cvent.service.ICventAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.cvent.entity.PageSupport.DEFAULT_PAGE_SIZE;
import static org.cvent.entity.PageSupport.FIRST_PAGE_NUM;

@Slf4j
@RestController
@RequestMapping("/api/cvent")
public class CventAccountController extends BaseController {

    @Autowired
    private ICventAccountService cventAccountService;

    @GetMapping(value = "/all")
    public Mono<ResponseEntity<ResponseBean>> getAll() {
        return processResponse(cventAccountService.getAllList());
    }

    @GetMapping(value = "/{id}")
    public Mono<ResponseEntity<ResponseBean>> getAllCventDataById(@PathVariable("id") Long id) {
        return processResponse(cventAccountService.getAllCventDataById(id));
    }

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<ResponseBean>> saveCventDetails(@RequestBody CventAccountEntity cventAccountEntity) {
        return processResponse(cventAccountService.saveCventDetails(cventAccountEntity));
    }

    @GetMapping(value = "/token/{id}")
    public Mono<ResponseEntity<ResponseBean>> getTokenById(@PathVariable("id") Long id) {
        return processResponse(cventAccountService.getId(id));
    }



}
