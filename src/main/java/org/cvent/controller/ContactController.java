package org.cvent.controller;


import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.ResponseBean;
import org.cvent.entity.ContactsEntity;
import org.cvent.service.IContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/cvent/contacts")
public class ContactController extends BaseController {

    @Autowired
    private IContactsService contactsService;

    @PostMapping(value = "/add")
    public Mono<ResponseEntity<ResponseBean>> saveContacts(@RequestBody ContactsEntity contactsEntity,
                                                           @RequestParam(value = "accessId", required = false) Long accessId) {
        return processResponse(contactsService.saveContacts(contactsEntity, accessId));
    }

    @GetMapping(value = "/all")
    public Mono<ResponseEntity<ResponseBean>> getContacts(
            @RequestParam(value = "accessId", required = false) Long accessId) {
        return processResponse(contactsService.getContacts(accessId));
    }


}
