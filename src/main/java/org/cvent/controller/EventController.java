package org.cvent.controller;


import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.ResponseBean;
import org.cvent.service.IEventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/cvent/event")
public class EventController extends BaseController {

    @Autowired
    private IEventsService eventsService;


    @GetMapping(value = "/list")
    public Mono<ResponseEntity<ResponseBean>> getEvents(
            @RequestParam(value = "accessId", required = false) Long accessId,
            @RequestParam(value = "isUpdate", required = false, defaultValue = "false") Boolean isUpdate) {
        return processResponse(eventsService.getEvents(accessId, isUpdate));
    }

    @GetMapping(value = "/admision_items")
    public Mono<ResponseEntity<ResponseBean>> getEventAdmissionItems(
            @RequestParam(value = "accessId", required = false) Long accessId) {
        return processResponse(eventsService.getEventAdmissionItems(accessId));
    }
}
