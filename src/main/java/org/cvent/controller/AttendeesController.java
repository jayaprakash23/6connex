package org.cvent.controller;


import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.ResponseBean;
import org.cvent.service.IAttendeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/cvent/attendees")
public class AttendeesController extends BaseController {

    @Autowired
    private IAttendeesService attendeesService;


    @GetMapping(value = "/all")
    public Mono<ResponseEntity<ResponseBean>> getAttendees(
            @RequestParam(value = "accessId", required = false) Long accessId,
            @RequestParam(value = "isUpdate", required = false, defaultValue = "false") Boolean isUpdate) {
        return processResponse(attendeesService.getAttendees(accessId, isUpdate));
    }

}
