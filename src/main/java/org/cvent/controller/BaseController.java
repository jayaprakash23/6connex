package org.cvent.controller;

import org.cvent.bean.ResponseBean;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.cvent.config.Config.APPLICATION_SCHEDULER;

public class BaseController {

    /**
     * Convert Mono to Mono Response Entity
     *
     * @param response
     */
    public <T> Mono<ResponseEntity<ResponseBean>> processResponse(Mono<T> response) {
        return response
                .filter(Objects::nonNull)
                .map(resData -> {
                    ResponseBean resBean = new ResponseBean<T>();
                    resBean.setData(resData);
                    return resBean;
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.noContent().build())
                .subscribeOn(APPLICATION_SCHEDULER);
    }

    /**
     * Convert Flux to Mono Response Entity
     *
     * @param response
     */
    public <T> Mono<ResponseEntity<ResponseBean>> processResponse(Flux<T> response) {
        return response
                .filter(Objects::nonNull)
                .collectList()
                .map(resData -> {
                    ResponseBean resBean = new ResponseBean<T>();
                    resBean.setData(resData);
                    return resBean;
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.noContent().build())
                .subscribeOn(APPLICATION_SCHEDULER);
    }


}
