package org.cvent.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@Component
public class CoreWebFilter implements WebFilter {

    @Value("${cors.ui.url}")
    private String corsURL;

    private static final Logger logger = LoggerFactory.getLogger(CoreWebFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        logger.info("Filtering on...........................................................");
        exchange.getResponse()
                .getHeaders()
                .addAll("Access-Control-Allow-Headers", Arrays.asList(
                        "Access-Control-Allow-Origin",
                        "Access-Control-Allow-Methods",
                        "Access-Control-Allow-Credentials",
                        "Content-Type"));
        exchange.getResponse()
                .getHeaders()
                .setAccessControlAllowOrigin(corsURL);
        exchange.getResponse()
                .getHeaders()
                .setAccessControlAllowCredentials(Boolean.TRUE);
        exchange.getResponse()
                .getHeaders()
                .setAccessControlAllowMethods(Arrays.asList(HttpMethod.GET, HttpMethod.DELETE, HttpMethod.POST, HttpMethod.OPTIONS, HttpMethod.PUT));
        return chain.filter(exchange);
    }


}
