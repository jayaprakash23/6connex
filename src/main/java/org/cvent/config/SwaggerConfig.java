package org.cvent.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Swagger config class
 */

@Configuration
public class SwaggerConfig {

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket api() throws IOException, URISyntaxException {
        final List<Response> globalResponses = Arrays.asList(
                new ResponseBuilder()
                        .code("200")
                        .description("OK")
                        .build(),
                new ResponseBuilder()
                        .code("400")
                        .description("Bad Request")
                        .build(),
                new ResponseBuilder()
                        .code("500")
                        .description("Internal Error")
                        .build());
        final ApiInfo apiInfo = new ApiInfo("Cvent service API", new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("")))
                .lines()
                .collect(Collectors.joining(System.lineSeparator())),
                "1.0.0", "", new Contact("team", "", "Cvent.com"), "", "", Collections.emptyList());
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .globalResponses(HttpMethod.GET, globalResponses)
                .globalResponses(HttpMethod.POST, globalResponses)
                .globalResponses(HttpMethod.DELETE, globalResponses)
                .globalResponses(HttpMethod.PATCH, globalResponses)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.cc"))
                .build()
                .apiInfo(apiInfo);
    }
}

