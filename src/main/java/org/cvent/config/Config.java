package org.cvent.config;

import org.springframework.util.StringUtils;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

public class Config {

    public static final Scheduler APPLICATION_SCHEDULER = Schedulers.boundedElastic();
    public static final String dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String dateFormat = "yyyy-MM-dd";
    public static final String _OFFSET = "offset";
    public static final String _PAGE_SIZE = "pageSize";
    public static final String _PERCENT = "%";

    public static String getLikeParam(String str) {
        if (StringUtils.hasText(str)) {
            return _PERCENT + str + _PERCENT;
        }
        return str;
    }

    public static String getStartLikeParam(String str) {
        if (StringUtils.hasText(str)) {
            return str + _PERCENT;
        }
        return str;
    }
}
