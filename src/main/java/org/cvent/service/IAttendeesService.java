package org.cvent.service;

import reactor.core.publisher.Mono;

public interface IAttendeesService {
    Mono<Object> getAttendees(Long accessId, boolean isUpdate);

}
