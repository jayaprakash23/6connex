package org.cvent.service;

import org.cvent.entity.ContactsEntity;
import reactor.core.publisher.Mono;

public interface IContactsService {
    Mono<Object> getContacts(Long accessId);

    Mono<ContactsEntity> saveContacts(ContactsEntity contactsEntity, Long accessId);
}
