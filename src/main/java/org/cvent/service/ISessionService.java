package org.cvent.service;

import reactor.core.publisher.Mono;

public interface ISessionService {

    Mono<Object> getSession(Long accessId, boolean isUpdate);
}
