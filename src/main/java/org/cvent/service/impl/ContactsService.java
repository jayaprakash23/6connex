package org.cvent.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.attendee.GetAttendeeResponseBean;
import org.cvent.dao.ContactsRepository;
import org.cvent.dao.CventAccountRepository;
import org.cvent.entity.ContactsEntity;
import org.cvent.service.IContactsService;
import org.cvent.util.AppConstants;
import org.cvent.webclient.WebClientWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@Transactional
public class ContactsService implements IContactsService {

    @Autowired
    private CventAccountRepository cventAccountRepository;

    @Autowired
    private ContactsRepository contactsRepository;

    @Autowired
    private WebClientWrapper webClientWrapper;


    public Mono<ContactsEntity> saveContacts(ContactsEntity contactsEntity, Long accessId) {
        log.info("Request received to save attendees data request contactsEntity [{}]: and accessId [{}]:", accessId, contactsEntity);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
            Mono<ContactsEntity> contacts = webClientWrapper.postToContactAPI(AppConstants.CONTACT_GROUP, contactsEntity, ContactsEntity.class, token, HttpMethod.POST)
                    .flatMap(res -> {
                        ContactsEntity contact = new ContactsEntity();
                        log.info("saving contactsEntity data");
                        contact.setContactId(res.getContactId());
                        contact.setId(res.getId());
                        contact.setName(res.getName());
                        contact.setShortDescription(res.getShortDescription());
                        contact.setDescription(res.getDescription());
                        contact.setType(res.getType());
                        contact.setNote(res.getNote());
                        contact.setCreated(res.getCreated());
                        contact.setLastModified(res.getLastModified());
                        contact.setCreatedBy(res.getCreatedBy());
                        contact.setLastModifiedBy(res.getLastModifiedBy());
                        contactsRepository.save(contact).subscribe();
                        return Mono.just(contact);
                    });
            return contacts;

        });
    }


    public Mono<Object> getContacts(Long accessId) {
        log.info("Request received to get Contact info  data with accessId [{}]: ", accessId);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
            Mono<GetAttendeeResponseBean> response = webClientWrapper.getFromAPICvent(AppConstants.CONTACT_LIST, GetAttendeeResponseBean.class, token, HttpMethod.GET);
            return response;
        });
    }

}
