package org.cvent.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.session.GetSessionResponseBean;
import org.cvent.dao.CventAccountRepository;
import org.cvent.dao.SessionRepository;
import org.cvent.entity.CventAccountEntity;
import org.cvent.entity.SessionEntity;
import org.cvent.service.ISessionService;
import org.cvent.util.AppConstants;
import org.cvent.webclient.WebClientWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.cvent.util.AppConstants.*;
import static org.cvent.util.AppUtility.convertXmlGregorianToLocalDate;

@Slf4j
@Service
@Transactional
public class SessionService implements ISessionService {

    @Autowired
    private CventAccountRepository cventAccountRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private WebClientWrapper webClientWrapper;

    public Mono<Object> getSession(Long accessId, boolean isUpdate) {
        log.info("Request received to get Session info  data with sellerId [{}]: ", accessId);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
            String url = AppConstants.SESSION_LIST;
            Mono<GetSessionResponseBean> response = webClientWrapper.getFromAPICvent(url, GetSessionResponseBean.class, token, HttpMethod.GET);
            return response.flatMap(entity -> populateSession(entity, accessTokenEntity, isUpdate));
        });
    }


    private Mono<GetSessionResponseBean> populateSession(GetSessionResponseBean responseBean, CventAccountEntity accessTokenEntity, boolean isUpdate) {
        {
            responseBean.getData().forEach(val -> {
                SessionEntity sessionEntity = new SessionEntity();

                sessionEntity.setEventId(val.getEvent().getId().toUpperCase());
                sessionEntity.setClientId(accessTokenEntity.getClient_id());
                sessionEntity.setSessionId(val.getId().toUpperCase());
                sessionEntity.setDataTagCode(val.getDataTagCode());
                sessionEntity.setProductName(val.getTitle());
                if (val.getCode() != null) {
                    sessionEntity.setProductCode(val.getCode());
                }
                sessionEntity.setProductCode("CD");
                sessionEntity.setProductDescription(val.getDescription());
                if (val.getLocation() != null) {
                    sessionEntity.setSessionLocationName(val.getLocation().getName());
                }
                sessionEntity.setTimeZone(val.getTimezone());

                if (val.getStatus() != null) {
                    sessionEntity.setStatus(val.getStatus());
                }
                if (val.getType().getName() != null) {
                    sessionEntity.setProductType(val.getType().getName());
                }
                if (val.getStart() != null) {
                    sessionEntity.setStartTime(convertXmlGregorianToLocalDate(
                            val.getStart()));
                }
                if (val.getEnd() != null) {
                    sessionEntity.setEndTime(convertXmlGregorianToLocalDate(
                            val.getEnd()));
                }
                sessionEntity.setEventPlatform(val.getType().getName());
                sessionEntity.setPlatformSubAccount("sachin.narayana@streamlinedcommunications.com");
                sessionEntity.setPlatformCoHost(SL_PLATFORM_CO_HOST);
                sessionEntity.setPlatformEventId(SL_PLATFORM_EVENT_ID);
                sessionEntity.setParticipationDuration(SL_PARTICIPATION_DURATION);
                sessionEntity.setHoldRegFlow("N");


                sessionEntity.setRequestedAction((isUpdate ? SL_UPDATE : SL_CREATE));

                sessionEntity.setUpdatedBy(CVENT_POLLER);
                //set updated and created date
                sessionEntity.setUpdatedDate(LocalDateTime.now(ZoneId.of(GMT_TIME_ZONE)));


                sessionRepository.save(sessionEntity).subscribe();

            });
            return Mono.just(responseBean);
        }
    }

}
