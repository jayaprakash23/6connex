package org.cvent.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.attendee.GetAttendeeResponseBean;
import org.cvent.dao.CventAccountRepository;
import org.cvent.entity.CventAccountEntity;
import org.cvent.entity.SessionRegistrationEntity;
import org.cvent.service.IAttendeesService;
import org.cvent.util.AppConstants;
import org.cvent.webclient.WebClientWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.cvent.util.AppConstants.*;
import static org.cvent.util.AppUtility.convertXmlGregorianToLocalDate;

@Slf4j
@Service
@Transactional
public class AttendeesService implements IAttendeesService {

    @Autowired
    private CventAccountRepository cventAccountRepository;

    @Autowired
    private org.cvent.dao.SessionRegistrationRepository sessionRegistrationRepository;

    @Autowired
    private WebClientWrapper webClientWrapper;

    public Mono<Object> getAttendees(Long accessId, boolean isUpdate) {
        log.info("Request received to get Attendees info  data with sellerId [{}]: ", accessId);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
            String url = AppConstants.ATTENDEES_LIST;
            Mono<GetAttendeeResponseBean> response = webClientWrapper.getFromAPICvent(url, GetAttendeeResponseBean.class, token, HttpMethod.GET);
            return response.flatMap(entity -> populateAttendee(entity, accessTokenEntity, isUpdate));
        });
    }


    private Mono<GetAttendeeResponseBean> populateAttendee(GetAttendeeResponseBean responseBean, CventAccountEntity accessTokenEntity, boolean isUpdate) {
        {
            responseBean.getData().forEach(val -> {
                SessionRegistrationEntity sessionRegistrationEntity = new SessionRegistrationEntity();
                if (sessionRegistrationEntity == null) {
                    sessionRegistrationEntity = new SessionRegistrationEntity();
                }

                sessionRegistrationEntity.setClientId(accessTokenEntity.getClient_id());
                sessionRegistrationEntity.setEventId(val.getEvent().getId().toUpperCase());
                sessionRegistrationEntity.setInviteeId(val.getInvitationList().getId().toUpperCase());

                sessionRegistrationEntity.setOrderDetailId(val.getConfirmationNumber());

                sessionRegistrationEntity.setFirstName(val.getContact().getFirstName());
                sessionRegistrationEntity.setLastName(val.getContact().getLastName());
                sessionRegistrationEntity.setCompany(val.getContact().getCompany());
                sessionRegistrationEntity.setTitle(val.getContact().getTitle());
                sessionRegistrationEntity.setEmailAddress(val.getContact().getEmail());
                sessionRegistrationEntity.setCcEmailAddress(val.getContact().getCcEmail());
                sessionRegistrationEntity.setContactId(val.getContact().getId());
                sessionRegistrationEntity.setSourceId(val.getContact().getSourceId());
                if (val.getRegistrationType() != null) {
                    sessionRegistrationEntity.setRegistrationType(val.getRegistrationType().getName());
                }
                //product info
                sessionRegistrationEntity.setProductDescription("Test");
                if (val.getAdmissionItem() != null) {
                    sessionRegistrationEntity.setProductId(val.getAdmissionItem().getId().toUpperCase());
                }
                sessionRegistrationEntity.setProductId("123");
                if (val.getContact() != null) {
                    sessionRegistrationEntity.setProductName(val.getContact().getTitle());
                }
                sessionRegistrationEntity.setProductName("Architect");

        /* registrationEntity.setParticipant(detail.isParticipant());
        registrationEntity.setOrderDetailItemId(detail.getOrderDetailItemId());
        registrationEntity.setOrderNumber(detail.getOrderNumber());
        registrationEntity.setQuantity(detail.getQuantity());
        registrationEntity.setProductCode(detail.getProductCode());
        registrationEntity.setProductType(detail.getProductType());*/
                sessionRegistrationEntity.setEventPlatform(val.getContact().getDesignation());
                if (_CANCELLED.equalsIgnoreCase(val.getStatus())) {
                    sessionRegistrationEntity.setRequestedAction(SL_DELETE);
                } else {
                    sessionRegistrationEntity.setRequestedAction(isUpdate ? SL_UPDATE : SL_CREATE);
                }
                //get from mapping table
                sessionRegistrationEntity.setStartTime(convertXmlGregorianToLocalDate(
                        val.getCreated()));
                sessionRegistrationEntity.setEndTime(convertXmlGregorianToLocalDate(
                        val.getLastModified()));
                sessionRegistrationEntity.setStatus(val.getStatus());

                if (sessionRegistrationEntity != null) {
                    if (val.getContact().getWorkAddress() != null) {
                        sessionRegistrationEntity.setWorkPhone(val.getContact().getWorkPhone());
                        sessionRegistrationEntity.setImageURL(val.getWebsiteUrl());
                        sessionRegistrationEntity.setAddress1(val.getContact().getWorkAddress().getAddress1());
                        sessionRegistrationEntity.setAddress2(val.getContact().getWorkAddress().getAddress2());
                        sessionRegistrationEntity.setWorkCity(val.getContact().getWorkAddress().getCity());
                        sessionRegistrationEntity.setWorkPostalCode(val.getContact().getWorkAddress().getPostalCode());
                        sessionRegistrationEntity.setWorkState("Test");
                        sessionRegistrationEntity.setCountry(val.getContact().getWorkAddress().getCountry());
                    }
                    if (val.getQuestions() != null) {
                        SessionRegistrationEntity sessionRegistration = sessionRegistrationEntity;
                        val.getQuestions().forEach(que -> {
                            sessionRegistration.setSurveyQuestion1(que.getName());
                            sessionRegistration.setSurveyQuestion2(que.getName());
                            sessionRegistration.setSurveyQuestion3(que.getName());
                            sessionRegistration.setSurveyQuestion4(que.getName());
                            sessionRegistration.setSurveyQuestion5(que.getName());
                            sessionRegistration.setSurveyQuestion6(que.getName());
                            sessionRegistration.setSurveyQuestion7(que.getName());

                        });
                    }
                }

                //set updated and created date
                sessionRegistrationEntity.setUpdatedBy(val.getLastModifiedBy());
        /*registrationEntity.setCreatedBy(CVENT_POLLER);
        registrationEntity.setUpdatedFlag(CVENT_UPDATE_FLAG);
        registrationEntity.setCreatedDate(LocalDateTime.now(ZoneId.of(GMT_TIME_ZONE)));*/
                sessionRegistrationEntity.setUpdatedDate(LocalDateTime.now(ZoneId.of(GMT_TIME_ZONE)));

                sessionRegistrationRepository.save(sessionRegistrationEntity).subscribe();
            });
            return Mono.just(responseBean);
        }
    }

}
