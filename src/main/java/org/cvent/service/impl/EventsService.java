package org.cvent.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.event.GetAdmissionResponse;
import org.cvent.bean.event.GetEventResponseBean;
import org.cvent.dao.CventAccountRepository;
import org.cvent.dao.EventRepository;
import org.cvent.entity.CventAccountEntity;
import org.cvent.entity.EventEntity;
import org.cvent.service.IEventsService;
import org.cvent.util.AppConstants;
import org.cvent.webclient.WebClientWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.cvent.util.AppConstants.*;
import static org.cvent.util.AppUtility.convertXmlGregorianToLocalDate;


@Slf4j
@Service
@Transactional
public class EventsService implements IEventsService {

    @Autowired
    private CventAccountRepository cventAccountRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private WebClientWrapper webClientWrapper;

    public Mono<Object> getEvents(Long accessId, boolean isUpdate) {
        log.info("Request received to get Events info  data with sellerId [{}]: ", accessId);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
            String after = "2017-01-02T02:00:00Z";
            String before = "2022-01-02T02:00:00Z";
            String url = AppConstants.EVENT_LIST + FILTER_AFTER + after + FILTER_BEFORE + before;
            Mono<GetEventResponseBean> response = webClientWrapper.getFromAPICvent(url, GetEventResponseBean.class, token, HttpMethod.GET);
            return response.flatMap(entity -> processEvent(entity, accessTokenEntity, isUpdate));
        });

    }


    private Mono<GetEventResponseBean> processEvent(GetEventResponseBean responseBean, CventAccountEntity accessTokenEntity, boolean isUpdate) {
        {
            responseBean.getData().forEach(val -> {
                EventEntity event = new EventEntity();
                //set updated and created date
                event.setUpdatedDate(LocalDateTime.now(ZoneId.of(GMT_TIME_ZONE)));
                if (!isUpdate) {
                    event.setCreatedDate(LocalDateTime.now(ZoneId.of(GMT_TIME_ZONE)));
                }
                event.setEventStub(val.getId());
                event.setEventCode(val.getCode());
                event.setClientId(accessTokenEntity.getClient_id());
                event.setEventTitle(val.getTitle());
                event.setEventStatus(val.getEventStatus());
                event.setPlanningStatus(val.getPlanningStatus());
                event.setEventStartDate(convertXmlGregorianToLocalDate(
                        val.getCreated()));
                event.setEventEndDate(convertXmlGregorianToLocalDate(
                        val.getEnd()));
                event.setLastModifiedDate(convertXmlGregorianToLocalDate(
                        val.getLastModified()));
                event.setTimeZone(val.getTimezone());
                event.setEventPlatform(val.getCategory().getName());

                event.setRequestedAction((isUpdate ? SL_UPDATE : SL_CREATE));
                event.setCreatedBy(CVENT_POLLER);
                event.setUpdatedBy(CVENT_POLLER);
                event.setUpdatedFlag(CVENT_UPDATE_FLAG);
                event.setTeamName(SL_TEAM_NAME);

                if (val.getCustomFields() != null) {
                    val.getCustomFields().forEach(e -> {
                        event.setSlCustomField1(e.getName());
                    });
                } else {
                    event.setSlCustomField1(SL_CUSTOM_FIELD1);
                }
                if (val.getPlaners() != null) {
                    val.getPlaners().forEach(p -> {
                        event.setPlannerFirstName(p.getFirstName());
                        event.setPlannerLastName(p.getLastName());
                        event.setPlannerEmailAddress(p.getEmail());
                    });
                }
                if (val.getVenues() != null) {
                    val.getVenues().forEach(address -> {
                        if (address.getAddress()!=null) {
                            event.setStreetAddress1(address.getAddress().getAddress1());
                            event.setStreetAddress2(address.getAddress().getAddress2());
                            event.setStreetAddress3(address.getAddress().getAddress3());
                        }
                    });
                }
                eventRepository.save(event).subscribe();

            });
            return Mono.just(responseBean);
        }
    }

    public Mono<Object> getEventAdmissionItems(Long accessId) {
        log.info("Request received to get Events Admission Items info  data with sellerId [{}]: ", accessId);
        return cventAccountRepository.findById(accessId).flatMap(accessTokenEntity -> {
            String token = accessTokenEntity.getAccessToken();
//            XMLGregorianCalendar after = null;
//            ZonedDateTime nowTime = null;
//            ZonedDateTime initiateTime = null;
//            ZonedDateTime startTime = null;
//            nowTime = ZonedDateTime.now(ZoneId.of(GMT_TIME_ZONE));
//            initiateTime = cachePolling.getLastRunDate(clientId, objectType.value()).getLastRunTime();
//            startTime = (initiateTime == null ? nowTime.minusSeconds(180) :
//                    (objectType.equals(CvObjectType.REGISTRATION) ? initiateTime.minusSeconds(seconds) :
//                            initiateTime.minusMinutes(minutes)));
//
//            after = convertDate(nowTime);
            String before = "2022-01-02T02:00:00Z";
            String after = "2017-01-02T02:00:00Z";
            String url = AppConstants.EVENT_LIST_ADMISION_ITES + FILTER_AFTER + after + FILTER_BEFORE + before;
            Mono<GetAdmissionResponse> response = webClientWrapper.getFromAPICvent(url, GetAdmissionResponse.class, token, HttpMethod.GET);
            return response;
        });
    }

}
