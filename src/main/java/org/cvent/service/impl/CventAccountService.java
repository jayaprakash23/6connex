package org.cvent.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.cvent.bean.login.CodeBean;
import org.cvent.bean.login.TokenBean;
import org.cvent.bean.login.TokenEntity;
import org.cvent.dao.AttendeesRepository;
import org.cvent.dao.ContactsRepository;
import org.cvent.dao.CventAccountRepository;
import org.cvent.entity.CventAccountEntity;
import org.cvent.service.ICventAccountService;
import org.cvent.util.AppConstants;
import org.cvent.webclient.WebClientWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.MINUTES;

@Slf4j
@Service
@Transactional
@Component
public class CventAccountService implements ICventAccountService {

    private static final int MILLIS_PER_MINUTE = 30000; //1 minute


    @Autowired
    private CventAccountRepository cventAccountRepository;

    @Autowired
    private ICventAccountService cventAccountService;


    @Autowired
    private AttendeesRepository attendeesRepository;

    @Autowired
    private ContactsRepository contactsRepository;

    @Value("${cvent.token.generate.url}")
    private String tokenGitURL;

    @Value("${cvent.scope}")
    private String scope;

    @Autowired
    private WebClientWrapper webClientWrapper;
    static final ObjectMapper mapperObj = new ObjectMapper();

    public Flux<CventAccountEntity> getAllList() {
        log.info("Request received in Service for Token ");
        return cventAccountRepository.findAll();
    }

    public Mono<CventAccountEntity> getAllCventDataById(Long id) {
        return cventAccountRepository.findById(id);
    }

    public Mono<CventAccountEntity> saveCventDetails(CventAccountEntity cventAccountEntity) {
        log.info("Request received to save cventAccountEntity data request [{}]:", cventAccountEntity);
        return cventAccountRepository.save(cventAccountEntity);

    }

    public Mono<Object> getId(Long id) {
        log.info("Request received to get token info  data with id [{}]: ", id);
        return cventAccountRepository.findById(id).flatMap(entity -> {

            TokenBean response = new TokenBean();
            response.setId(entity.getId());
            response.setClient_id(entity.getClient_id());
            response.setClient_secret(entity.getClient_secret());
            response.setCreatedDate(LocalDateTime.now());
            response.setGrant_type(AppConstants.GRANT_TYPE_CLIENT);
            response.setScope(scope);
            return Mono.just(response).flatMap(clientRes -> {

                CodeBean clientPayload = new CodeBean();
                clientPayload.setGrant_type(clientRes.getGrant_type());
                clientPayload.setScope(clientRes.getScope());

                // create headers
                String authStr = (clientRes.getClient_id() + ":" + clientRes.getClient_secret());
                String clientDetailsHeader = Base64.getEncoder().encodeToString(authStr.getBytes());

                Mono<CventAccountEntity> token = webClientWrapper.postToTokenAPI(tokenGitURL, clientPayload.toString(), TokenEntity.class, clientDetailsHeader, HttpMethod.POST)
                        .flatMap(tokenEntity -> {
                            CventAccountEntity accessTokenEntity = new CventAccountEntity();
                            log.info("saving client access token data");
                            //token return values
                            accessTokenEntity.setAccessToken(tokenEntity.getAccess_token());
                            accessTokenEntity.setExpiresIn(tokenEntity.getExpires_in());
                            accessTokenEntity.setTokenType(tokenEntity.getToken_type());
                            //entity return values
                            accessTokenEntity.setRefresh_token(tokenEntity.getRefresh_token());
                            accessTokenEntity.setId(response.getId());
                            accessTokenEntity.setGrant_type(response.getGrant_type());
                            accessTokenEntity.setClient_secret(response.getClient_secret());
                            accessTokenEntity.setClient_id(response.getClient_id());
                            accessTokenEntity.setScope(scope);
                            accessTokenEntity.setCreatedBy("USER");
                            accessTokenEntity.setUpdatedBy("USER");
                            accessTokenEntity.setUpdatedDate(LocalDateTime.now());
                            cventAccountRepository.save(accessTokenEntity).subscribe();
                            return Mono.just(accessTokenEntity);
                        });
                return token;
            });
        });
    }

    @Scheduled(fixedDelay = MILLIS_PER_MINUTE)
    public void startAuthService() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        Flux<CventAccountEntity> accessTokenEntity = null;
        accessTokenEntity = cventAccountService.getAllList();
        accessTokenEntity.
                flatMap(cventAuthEntity -> {
                    LocalDateTime lastUpdateDate = cventAuthEntity.getUpdatedDate();
                    long seconds = MINUTES.between(lastUpdateDate, currentDateTime);
                    long expiredInMins = TimeUnit.MINUTES.toMinutes(Long.valueOf(cventAuthEntity.getExpiresIn()));//30 minutes
                    long currentState = TimeUnit.MINUTES.toMinutes(seconds);

                    if (currentState > expiredInMins - 1200) { //before 10 minutes of expiry
                        log.info("Time to update the Token Code and calling APIs");
                        updateCventToken(cventAuthEntity);
                    } else {
                        log.info("Not Time to update Code");
                    }
                    return Mono.just(cventAuthEntity);
                }).subscribe();
    }


    public String updateCventToken(CventAccountEntity cventAuthEntity) {
        log.info("=============Inside Update Cvent Token method=====================");
        CodeBean clientPayload = new CodeBean();
        clientPayload.setGrant_type(cventAuthEntity.getGrant_type());
        clientPayload.setScope(scope);
        // create headers
        String authStr = (cventAuthEntity.getClient_id() + ":" + cventAuthEntity.getClient_secret());
        String clientDetailsHeader = Base64.getEncoder().encodeToString(authStr.getBytes());
        Disposable token = webClientWrapper.postToTokenAPI(tokenGitURL, clientPayload.toString(), TokenEntity.class, clientDetailsHeader, HttpMethod.POST)
                .flatMap(tokenEntity -> {
                    log.info("saving update client access token data");
                    CventAccountEntity accessTokenEntity = new CventAccountEntity();
                    //token return values
                    accessTokenEntity.setAccessToken(tokenEntity.getAccess_token());
                    accessTokenEntity.setExpiresIn(tokenEntity.getExpires_in());
                    accessTokenEntity.setTokenType(tokenEntity.getToken_type());
                    //entity return values
                    accessTokenEntity.setRefresh_token(tokenEntity.getRefresh_token());
                    accessTokenEntity.setId(cventAuthEntity.getId());
                    accessTokenEntity.setGrant_type(cventAuthEntity.getGrant_type());
                    accessTokenEntity.setClient_secret(cventAuthEntity.getClient_secret());
                    accessTokenEntity.setClient_id(cventAuthEntity.getClient_id());
                    accessTokenEntity.setScope(scope);
                    accessTokenEntity.setCreatedBy("CVENT");
                    accessTokenEntity.setUpdatedBy("CVENT");
                    accessTokenEntity.setUpdatedDate(LocalDateTime.now());
                    cventAccountRepository.save(accessTokenEntity).subscribe();
                    return Mono.just(accessTokenEntity);
                }).subscribe();
        return String.valueOf(cventAuthEntity);
    }


    @Cacheable(value = "cvent.token", key = "#entity.id", unless = "#result == null")
    public String getLoginToken(CventAccountEntity entity) {
        System.out.println("eCache" + entity);
        return updateCventToken(entity);
    }


}
