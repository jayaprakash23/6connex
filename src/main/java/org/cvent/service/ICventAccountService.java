package org.cvent.service;

import org.cvent.entity.AttendeesEntity;
import org.cvent.entity.ContactsEntity;
import org.cvent.entity.CventAccountEntity;
import org.cvent.entity.PageSupport;
import org.springframework.data.domain.PageRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICventAccountService {

    Mono<CventAccountEntity> getAllCventDataById(Long id);

    Mono<CventAccountEntity> saveCventDetails(CventAccountEntity cventAccountEntity);

    Mono<Object> getId(Long id);

    Flux<CventAccountEntity> getAllList();

}
