package org.cvent.service;

import reactor.core.publisher.Mono;

public interface IEventsService {
    Mono<Object> getEvents(Long accessId, boolean isUpdate);

    Mono<Object>  getEventAdmissionItems(Long accessId);
}
