package org.cvent.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import static org.cvent.util.AppConstants.EVENT_DATE_FORMAT;
import static org.cvent.util.AppConstants.GMT_TIME_ZONE;

@Slf4j
public class AppUtility {
    public static final String _PERCENT = "%";
    public static final DateTimeFormatter appFormatter = DateTimeFormatter.ofPattern(EVENT_DATE_FORMAT);
    public static String getLikeParam(String str) {
        if (StringUtils.hasText(str)) {
            return _PERCENT + str + _PERCENT;
        }
        return str;
    }

    static final ObjectMapper mapperObj = new ObjectMapper();

    /**
     * Method to convert object to string
     *
     * @param obj
     * @return
     */
    public static String convertObjectToString(Object obj) {
        String resultStr = null;
        try {
            resultStr = mapperObj.writeValueAsString(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    /**
     * Convert json to VECreateRegistrant Response Bean Object
     *
     * @param jsonString
     * @return
     */
    public static <T> T convertJsonToObj(String jsonString, Class<T> tClass) {
        T obj = null;
        try {
            obj = mapperObj.readValue(jsonString, tClass);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error while converting json string to java object [{}]:", jsonString);
        }
        return obj;
    }


    /**
     * @param jsonString
     * @return
     */
    public static List<String> convertToStringObj(String jsonString) {
        List<String> obj = null;
        try {
            mapperObj.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            obj = mapperObj.readValue(jsonString, new TypeReference<List<String>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error while converting json string to list string object [{}]:", jsonString);
        }
        return obj;
    }

    /**
     * @param jsonString
     * @return
     */
    public static List<Long> convertToLongObj(String jsonString) {
        List<Long> obj = null;
        try {
            mapperObj.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            obj = mapperObj.readValue(jsonString, new TypeReference<List<Long>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error while converting json string to list string object [{}]:", jsonString);
        }
        return obj;
    }

    /**
     * Method to check if string is null
     *
     * @param strInput
     * @return
     */
    public static boolean isNullOrEmpty(String strInput) {
        if (strInput == null || (strInput != null && strInput.trim().length() == 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to check if long value is null or zero
     *
     * @param id
     * @return
     */
    public static boolean isNullOrZero(Long id) {
        if (id == null || (id != null && id == 0)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Method to convert the Zone Date Time to String
     * @param dateTime
     * @return
     */
    public static String dateFormatter(ZonedDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern (EVENT_DATE_FORMAT);
        return formatter.format(dateTime.withZoneSameInstant(ZoneId.of(GMT_TIME_ZONE)));
    }
    public static XMLGregorianCalendar convertDate(ZonedDateTime dateTime) {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateFormatter(dateTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlGregorianCalendar;
    }
    /**
     * Convert XMLGregorianCalender to current date
     * @param xc
     * @return
     */
    public static LocalDateTime convertXmlGregorianToLocalDate(XMLGregorianCalendar xc){
        GregorianCalendar gCalendar = xc.toGregorianCalendar();
        gCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat df = new SimpleDateFormat(EVENT_DATE_FORMAT);
        return LocalDateTime.parse(df.format(gCalendar.getTimeInMillis()));
    }

    /**
     * Convert XMLGregorianCalender to current date
     * @param xc
     * @return
     */
    public static String convertDateForCompare(XMLGregorianCalendar xc){
        return appFormatter.format(convertXmlGregorianToLocalDate(xc));
    }


}


