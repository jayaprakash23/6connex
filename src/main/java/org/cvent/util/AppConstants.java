package org.cvent.util;

public class AppConstants {

    public static final String HYPEN = "-";
    public static final String FORWARD_SLASH = "/";
    public static final String EQUAL = "=";
    public static final String AND = "&";
    public static final int batchSize = 50;
    public static final String GMT_TIME_ZONE = "GMT";
    public static final String GRANT_TYPE = "refresh_token";
    public static final int minutes = 10;
    public static final int seconds = 20;
    public static final String SL_CREATE = "CREATE";
    public static final String SL_UPDATE = "UPDATE";
    public static final String CVENT_POLLER = "CVENT_POLLER";
    public static final String SL_PLATFORM_CO_HOST = "SL_PLATFORM_CO_HOST";
    public static final String SL_PLATFORM_EVENT_ID = "SL_PLATFORM_EVENT_ID";
    public static final String SL_PARTICIPATION_DURATION = "SL_PARTICIPATION_DURATION";
    public static final String _CANCELLED = "Cancelled";
    public static final String SL_DELETE = "DELETE";
    public static final String CVENT_UPDATE_FLAG = "N";
    public static final String EVENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String SL_CUSTOM_FIELD1 = "SL_CUSTOM_FIELD1";
    public static final String SL_TEAM_NAME = "Pearson Team";
    public static final String NEW_TOKEN_GRANT_TYPE = "authorization_code";
    public static final String GRANT_TYPE_CLIENT = "client_credentials";
    public static final String REDIRECT_URL = "https://0c76-2409-4072-8e86-5889-cd21-804d-9912-b3cd.ngrok.io/api/cvent/query";
    public static final String ATTENDEES_POST_URL = "https://api-platform.cvent.com/ea/attendees";
    public static final String CLIENT_ID = "2u12gpsmpk417kait3f8kgdrea";
    public static final String ATTENDEES_LIST = "https://api-platform.cvent.com/ea/attendees";
    public static final String CLIENT_SECRET = "42nvabf4v5vt9r84koa17knihbsppi0b0d6rd6agb716ro8i3";
    public static final String CONTACT_LIST = "https://api-platform.cvent.com/ea/contacts";
    public static final String CONTACT_GROUP = "https://api-platform.cvent.com/ea/contact-groups";
    public static final String EVENT_LIST = "https://api-platform.cvent.com/ea/events";
    public static final String EVENT_LIST_ADMISION_ITES = "https://api-platform.cvent.com/ea/admission-items";

    public static final String FILTER_AFTER ="?after=";
    public static final String FILTER_BEFORE ="&?before=";

    public static final String SESSION_LIST = "https://api-platform.cvent.com/ea/sessions";

}
