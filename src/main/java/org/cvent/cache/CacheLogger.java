package org.cvent.cache;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.instrument.Instrumentation;

import static java.util.Objects.isNull;

@Component
public class CacheLogger implements CacheEventListener<Object, Object> {
    private final Logger LOG = LoggerFactory.getLogger(CacheLogger.class);

    private static volatile Instrumentation globalInstrumentation;

    public static void premain(final String agentArgs, final Instrumentation inst) {
        globalInstrumentation = inst;
    }

    public long getObjectSize(Object o) {
        if(o==null){
            return 0;
        }
        return globalInstrumentation.getObjectSize(o);
    }

    @Override
    public void onEvent(CacheEvent<?, ?> cacheEvent) {
        LOG.info("Key: {} | EventType: [{}] | Old value: [{}] | New value: [{}] | Size [{}]",
                cacheEvent.getKey(), cacheEvent.getType(), (!isNull(cacheEvent.getOldValue())?"Old Value":""),
                (!isNull(cacheEvent.getNewValue())?"New Value Added":""), "NA");
    }
}