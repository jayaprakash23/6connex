package org.cvent.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CacheService {
    private static final Logger log = LoggerFactory.getLogger(CacheService.class);

//    @Autowired
//    private EventRepository eventRepository;
//
//
//    /*
//        Event Level Methods
//     */
//    @Cacheable(value = "event.id", key = "#eventStub.concat('-').concat(#clientId)", unless = "#result == null")
//    public EventEntity getEventByClientIdAndEventId(Long clientId, String eventStub) {
//        log.info("Calling Event Cache for clientId [{}] and EventStub [{}] ", clientId, eventStub);
//        return eventRepository.findByEventStubAndClientId(eventStub, clientId).orElse(null);
//    }
//
//    @CacheEvict(value = "event.id", key = "#eventStub.concat('-').concat(#clientId)")
//    public void removeEventByClientIdAndEventId(Long clientId, String eventStub) {
//        log.info("Remove Event Cache for clientId [{}] and EventStub [{}] ", clientId, eventStub);
//    }


}
